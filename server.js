const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const app = express();
const server = http.Server(app);
const io = socketIO(server);
const cookieParser = require('cookie-parser')
const secure = require('express-force-https');
app.use(cookieParser());
//app.use(secure);
app.disable('x-powered-by');


const ServerHelper = require('./backend/server-helper.js');

const util = require('./backend/util.js');
const serverHelper = new ServerHelper(io);

let port = process.env.PORT;
if (port == null || port == "") {
  port = 5000;
}
const enableDebug = true;


app.use('/javascript', express.static(__dirname + '/javascript'));
app.use('/javascript/socket.io.js', express.static(__dirname + '/node_modules/socket.io-client/dist/socket.io.js')); // Add socket.io to js dir

app.use('/assets', express.static(__dirname + '/assets'));

app.use('/stylesheets', express.static(__dirname + '/stylesheets'));



// Index page
app.get('/', function(request, response) {
    response.sendFile(__dirname + '/views/index.html');
});



app.get('/create', function(request, response) { //XXX app.post() not working for some reason -_-
    if(request.query.username && request.query.deckLength){
        let gamecode = serverHelper.createLobby(request.query.deckLength);
        joinLobby(gamecode, request.query.username, response, true);
    }else{
        errorhandler(response, "parameters");
    }
});

app.get('/join', function(request, response) { //XXX app.post() not working for some reason -_-
    if(request.query.username && request.query.gamecode){    
        response
            .clearCookie('token')
            .clearCookie('usertoken')
            .clearCookie('game');    
        let action = {
            new: () => joinLobby(request.query.gamecode, request.query.username, response, false),
        };
        router(request, response, action, request.query.gamecode);
    }else{ //parameter wrong
        errorhandler(response, "parameters");
    }
});

app.get('/join/:code', function(request, response) {
    let action = {
        new: () => response.sendFile(__dirname + '/views/index.html'),
    };
    router(request, response, action);
});




app.get('/lobby/:code', function(request, response) {
    let action = {
        lobby: () => response.sendFile(__dirname + '/views/lobby.html'),
    };
    router(request, response, action);
});



app.get('/game/:code', function(request, response) {
    let action = {
        game: () => response.sendFile(__dirname + '/views/game.html'),
    };
    router(request, response, action);
});


//XXX debug
if(enableDebug){

    app.get('/debug', function(request, response) {
        response.json(serverHelper.debug());
    });
    app.get('/debug/:code', function(request, response) {
        response.json(serverHelper.debug(request.params.code));
    });
    
    const uptime = new Date();
    app.get('/uptime', function(request, response) {
        response.send("Script execution started on: "+uptime);
    });

}


// Create 404 page
app.use(function (request, response, next) {
    errorhandler(response, "404");
})


// Start the server
server.listen(port, function() {
    console.log("Starting server on port "+port);
});




/*
token: Used to reconnect from the same browser and for identification in the server script
usertoken: as identifier with other users
*/
function joinLobby(gamecode, username, response, createdGame){ //called by /join and /create
    response
        .clearCookie('token')
        .clearCookie('usertoken')
        .clearCookie('game');

    let exp = {expires: new Date(Date.now() + 8 * 3600000)} // cookie valid for 8h
    let tokens = serverHelper.joinLobby(gamecode, username, false, false, createdGame);
    response
        .cookie("token", tokens[0], exp)
        .cookie("usertoken", tokens[1], exp)
        .cookie("game", gamecode, exp)
        .redirect('/lobby/'+gamecode);
}




/*
let action = {
    new: () => joinLobby(request.query.gamecode, request.query.username, response),
    lobby: () => response.sendFile(__dirname + '/views/lobby.html'),
    game: () => response.sendFile(__dirname + '/views/game.html'),
};
*/
function router(request, response, action={}, gamecode=request.params.code){
    //main purpose check cookies and put user in the right place. No cookies-> not joined.
    gamecode = gamecode.toLowerCase();
    if("game" in request.cookies && request.cookies.game==gamecode && "token" in request.cookies){
        let token = request.cookies.token;
        if(serverHelper.gameExists(gamecode) && token in serverHelper.activeGames[gamecode].players && serverHelper.activeGames[gamecode].players[token].ready){ //user known in game
            if("game" in action){
                action.game();
            }else{
                response.redirect('/game/'+gamecode);
            }
        }else if(serverHelper.lobbyExists(gamecode) && token in serverHelper.activeLobbies[gamecode].players){ //user known in lobby
            if("lobby" in action){
                action.lobby();
            }else{
                response.redirect('/lobby/'+gamecode);
            }
        }else if (serverHelper.lobbyExists(gamecode)){ // token unknown but they know the lobby code
            // clear cookies so user can connect if they were removed when disconnected
            response
                .clearCookie('token')
                .clearCookie('usertoken')
                .clearCookie('game');

            response.redirect('/join/'+gamecode);
        }else{ // game/lobby not exists
            errorhandler(response, "game_not_exists");
        }
    }else if(serverHelper.lobbyExists(gamecode)){
        if("new" in action){
            action.new();
        }else{
            response.redirect('/join/'+gamecode);
        }
    }else if("default" in action){ //XXX is this even needed? Do we have change the default response?
        action.default();
    }else{
        response.redirect('/');
    }
}



function errorhandler(response, err){
    response
            .status(404)
            .sendFile(__dirname + '/views/error/'+err+'.html');
}
