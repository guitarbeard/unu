function getGameCode(){
    let p = location.pathname.split("/"); //[0] is always empty
    if(2 in p) return p[2];
    return false;
}


// https://stackoverflow.com/a/15724300/4346956
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function setCookie(name, value, days) {
  var d = new Date;
  d.setTime(d.getTime() + 24*60*60*1000*days);
  document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

function deleteCookie(name) { setCookie(name, '', -1); }