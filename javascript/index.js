document.addEventListener("DOMContentLoaded", function() { 
    let gamecode = getGameCode();
    if (gamecode) {
        document.getElementById("gamecode").value = gamecode;
        document.getElementById("gamecode-label").classList = 'active';
        document.getElementById("username-input").focus();
    }
    
});
