//onload
document.addEventListener("DOMContentLoaded", function() { 
    
    document.getElementById("inviteLink").innerHTML = document.location.protocol+"//"+document.location.host+"/join/"+getGameCode();
    
});


const socket = io('/lobby/'+getGameCode());


inLobby();

socket.on('refresh', inLobby);


socket.on('playersUpdated', function(message) {
    var createdGame = message.find(player => player.usertoken === getCookie("usertoken")).createdGame;
    document.getElementById("players").innerHTML = "<ul>" + message.map(player => '<li><div class="chip' + (player.inLobby ? '' : ' away') + '"><span>' + player.name + '</span>' + (player.ready ? '<i class="material-icons">check</i>' : '') + ((createdGame && !player.createdGame) ? '<i class="material-icons close" token="' + player.token + '">close</i>' : '') + '</div></li>').join("")+"</ul>";
    if (createdGame) {
        var elems = document.querySelectorAll('.chip .close');
        for (var i = 0; i < elems.length; i++) {
            elems[i].addEventListener('click', function(ev){
                let token = ev.currentTarget.getAttribute('token');
                removePlayer(token);
            }, false);
        }
    }
});

socket.on('gameStarting', async function(message) {
    await sleep(500); //server has to set up new Game() and create new socket
    location.href = "/game/"+getGameCode();
    socket.disconnect();
});

socket.on('playerRemoved', function(token) {
    if(token === getCookie("token")){
        deleteCookie('usertoken');
        deleteCookie('token');
        location.href = "/";
        socket.disconnect();
    }
});

function inLobby(){
    socket.emit("inLobby", getCookie("token"));
}

function startGame(){ // Button clicked
    socket.emit("ready", getCookie("token"));
    return false;
}

function removePlayer(token){
    socket.emit("removePlayer", token);
    return false;
}

function copyLink(){
    var textArea = document.createElement("textarea");
    textArea.value = document.getElementById("inviteLink").textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    M.toast({html: 'LINk c0pi3D'});
    return false;
}
