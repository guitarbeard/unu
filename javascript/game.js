//onload
document.addEventListener("DOMContentLoaded", function() { 
        
    inGame();

    let drawpile = document.getElementById("draw");
    drawpile.addEventListener('click', drawNewCard);

    let endGameBtn = document.getElementById("end-game");
    endGameBtn.addEventListener('click', endGame);

    let unuBtn = document.getElementById("say-uno-btn");
    unuBtn.addEventListener('click', sayUnu);

    let colorPicker = document.getElementById("color-picker");
    colorPicker.addEventListener('submit', handleColorSubmit);

    let colorPickerBg = document.getElementById("color-picker-bg");
    colorPickerBg.addEventListener('click', function(){
        let colorPickerModal = document.getElementById("color-picker-modal");
        colorPickerModal.classList = '';
    });

    // end game modal
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {endingTop: '30%'});
});


const socket = io('/game/'+getGameCode());
let mycards = {};
let cardorder = [];
let rules = {};
let myturn = false;
let createdGame = false;
// stop David from double-clicking too fast!
let canHazNewCard = true;
let direction = 1;
const confettiSettings = {
    target: 'confetti',
    max: 1000,
    rotate: true,
  };
let confetti = null;


//XXX not used
socket.on('refresh', inGame);
socket.on('test', msg=>alert(msg));

socket.on('mycards', function(msg) {
    mycards=msg;
    cardorder = Object.keys(msg);
    updateCards();
});


socket.on('gameData', function(message) {
    // console.log(message);
    // reset after a turn
    canHazNewCard = true;
    direction = message['direction'];
    // Set player list
    document.getElementById("players").innerHTML = '<ul>' + message["players"].map(player => '<li class="player-btn-wrap' + (!player.inGame ? ' away' : '') + (player.saidUnu ? ' said-unu' : '') + (player.cards === 0 ? ' winner' : '') + '"><button tabindex="0" role="button" token="' + player.token + '" card-count="' + player.cards + '" class="player-btn chip ' + (player.current ? ' cyan white-text z-depth-2' : '') + '"><span>' + player.name + '</span>' + (player.current ?  (direction > 0 ? '<i class="material-icons forward">arrow_forward</i>' : '<i class="material-icons reverse">arrow_back</i>') : '') + (player.cards > 0 ? '<div class="red lighten-1 white-text card-count">'+player.cards+'</div>' : '') +'</button>' + (player.wins > 0 ? ('<span class="win-count">' + player.wins + ' ⭐' + '</span>') : '')+'</li>').join('')+"</ul>";
    
    // add click events to player btns
    let playerBtns = document.getElementsByClassName("player-btn");
    Array.from(playerBtns).forEach(function(playerBtn) {
        playerBtn.addEventListener('click', playerBtnClick);
    });

    // Set discard pile top card
    let discardpile = document.getElementById("discardpile");
    createCard(discardpile,message["discard"]);
    
    rules = message["rules"];
    
    myturn = message["players"].find(player => player.current).usertoken === getCookie("usertoken");
    createdGame = message["players"].find(player => player.usertoken === getCookie("usertoken")).createdGame;

    if (createdGame) {
        document.getElementById("end-game-modal-trigger").classList.add("active");
    }

    // Set current player message turn and WIN
    let turnTxt = document.getElementById("turn");
    turnTxt.classList = myturn ? "active z-depth-3" : "";
    if(confetti) {
        confetti.clear();
        confetti = null;
    }
    if (Object.keys(mycards).length === 0) {
        turnTxt.innerHTML = "<marquee>" + (myturn ? '⭐ Your Turn ' : '') + "⭐ YOU WIN!!! ⭐</marquee>";
        confetti = new ConfettiGenerator(confettiSettings);
        confetti.render();
    } else {
        turnTxt.innerHTML = "Your Turn";
    }

    // Set card count
    document.getElementById("card-count").innerHTML = message['drawpile'].length;
    if(!message['drawpile'].length){
        document.getElementById("draw").setAttribute("disabled",  true);
        document.getElementById("draw").classList.add('notavail');
    } else {
        document.getElementById("draw").removeAttribute("disabled");
        document.getElementById("draw").classList.remove('notavail');
    }
    
});

socket.on('timerUpdate', function(message) {
    myturn = message["players"].find(player => player.current).usertoken === getCookie("usertoken");
    var clock = document.getElementById("clock");
    clock.innerHTML = "";

    var time = message['time'];
    if(myturn) {
        clock.innerHTML = ':'+('0' + time).slice(-2);
        if (time === 0) {
            M.toast({html: 't00 SL0W'});
            clock.innerHTML = "";
        }
    }
});

socket.on('cardMessage', function(data) {
    myturn = data["players"].find(player => player.current).usertoken === getCookie("usertoken");

    if(myturn) {
        M.toast({html: data['message']});
    }
});

socket.on('gameFinished', async function(message) {
    await sleep(500); //server has to set up new Lobby() and create new socket
    location.href = "/lobby/"+getGameCode();
    socket.disconnect();
});


socket.on('disconnect', function(message) {
    //TODO block screen and notify about disconnection, add refreshbutton
    console.log("disconnected");
});


function inGame(){
    socket.emit("inGame", getCookie("token"));
}

function endGame(){
    socket.emit("endGame", getCookie("token"));
}

socket.on('gameEnding', async function(message) {
    await sleep(500); //server has to set up new Game() and create new socket
    location.href = "/lobby/"+getGameCode();
    socket.disconnect();
});

//XXX debug
// Creates basic 0-9 cards for debugging
let idcounter = 0;
function createDebugCard(){
    let colors = ["blue","green","yellow","red"];
    //mycards.push( { number: Math.floor(Math.random() * 10), color: colors[Math.floor(Math.random() * colors.length)], id: "cardid"+idcounter++} );
    mycards["cardid"+idcounter++] = { number: Math.floor(Math.random() * 10), color: colors[Math.floor(Math.random() * colors.length)]};
    updateCards();
}


function updateCards(){
    document.getElementById("mycards").innerHTML = "";
    let c = 0;
    createDroparea(c);
    for(let cardid of cardorder) {
        // create card
        let card = document.createElement('button')
        card.setAttribute("tabindex", "0")
        card.setAttribute("role", "button");
        card = createCard(card,mycards[cardid]);
        
        card.id = cardid;
        card.addEventListener("click", card_click);
        
        document.getElementById("mycards").appendChild(card);
        createDroparea(++c);
    }
}


// Create HTML structure of a card
function createCard(el,carddata){
    el.setAttribute("title", carddata["color"] + ' ' + (carddata["type"] === 'regular' ? carddata["number"] : carddata["type"].replace('wild', '').replace('draw', 'draw ')));
    el.className = 'card';
    el.dataset.number = carddata["number"];
    el.dataset.color = carddata["color"];
    el.innerHTML = "<span>"+(carddata["type"].startsWith("wild")?"?":carddata["number"])+"</span>"; //Set span to '?' for all wilds
    return el;
}



// Drop functions
function createDroparea(pos){
    let droparea = document.createElement('div');
    droparea.className = "droparea";
    droparea.dataset.pos = pos;
    droparea.addEventListener('dragover', allowdrop);
    droparea.addEventListener('drop', drop_reorder);
    document.getElementById("mycards").appendChild(droparea);
}


function allowdrop(ev){
    //only allow card drop, and not the surrounding droparea, and drawpile
    if(ev.dataTransfer.types[0]=="cardid" && ev.toElement && ev.toElement.classList.contains("dropoption")){
        ev.preventDefault();
        ev.dataTransfer.dropEffect = "move";
        
    }
    
}

function drop_reorder(ev){
     ev.preventDefault();
     let cardid = ev.dataTransfer.getData("cardid");
     let from = document.getElementById(cardid).previousElementSibling.dataset.pos;
     let to = ev.target.dataset.pos;
     to -= from<to?1:0; // get new position after removing card
     let thiscard = cardorder.splice(from,1)[0];
     cardorder.splice(to, 0, thiscard);
     
     updateCards();
}

function drawNewCard() {
    if(canHazNewCard && myturn && document.getElementById("draw").getAttribute("disabled") !== 'true'){
        canHazNewCard = false;
        var num = 1;
        if (Object.keys(mycards).length === 0) {
            num = 7;
        }
        socket.emit("addCardsToCurrentPlayer", num);
        socket.emit("nextplayer");
    } else {
        M.toast({html: 'h0Ld y00R H0r53z'});
    }
}

function handleColorSubmit(ev) {
    ev.preventDefault();
    let selectedColor = document.querySelector('#color-picker input[name="color"]:checked');
    let selectedCardid = document.querySelector('#color-picker').getAttribute('cardid');
    if (selectedColor) {
        // hide color picker
        let colorPickerModal = document.getElementById("color-picker-modal");
        colorPickerModal.classList = '';
        click_drop(selectedCardid, selectedColor.value);
    } else {
        M.toast({html: 'CH0023 4 c0l0r'});
    }    
}

function playerBtnClick(ev) {
    let cardCount = parseInt(ev.currentTarget.getAttribute('card-count'), 10);
    let token = ev.currentTarget.getAttribute('token');
    if (cardCount === 1 && !ev.currentTarget.parentNode.classList.contains("said-unu") && !ev.currentTarget.parentNode.classList.contains("away")) {
        socket.emit("addCardsToPlayer", {token: token, num: 4});
        M.toast({html: 'PUN1SH3D'});
    } else if (ev.currentTarget.parentNode.classList.contains("away")) {
        M.toast({html: 'cURR3ntLy 4w4Y'});
    } else if (ev.currentTarget.parentNode.classList.contains("said-unu")) {
        M.toast({html: 't00 SL0W'});
    } else if (cardCount > 1) {
        M.toast({html: 'h0Ld y00R H0r53z'});
    } 
}

function sayUnu() {
    if ((Object.keys(mycards).length === 2 && myturn) || Object.keys(mycards).length === 1) {
        socket.emit("sayUnu", true);
    } else {
        M.toast({html: 'h0Ld y00R H0r53z'})
    }    
}

function card_dragstart(ev){
    //FIXME detects text as drag if we start dragging from a cards
    if(!ev.target || !ev.target.classList || !ev.target.classList.contains("card")) return false;
    

    ev.dataTransfer.setData("cardid", ev.target.id);
    ev.dataTransfer.effectAllowed = "move";
    ev.target.classList.add("notavail");
    
    let dropareas = document.getElementsByClassName("droparea");
    for(da of dropareas){
        if(da.dataset.pos!=ev.target.previousElementSibling.dataset.pos && da.dataset.pos!=ev.target.nextElementSibling.dataset.pos){
            da.classList.add("dropoption");
        }
        
        if(is_drop_allowed(ev.target.dataset)){
            document.getElementById("discardpile").classList.add("dropoption");
        }
    }
}

function click_drop(cardid, selectedColor = false){

    cardorder.splice(cardorder.indexOf(cardid), 1); //Remove the selected card from cardorder

    let data = {
        id: cardid.replace(/\D/g,''),
        cardorder: cardorder.map(el=>el.replace(/\D/g,''))
    };
    if(mycards[cardid]["color"] === "wild" && selectedColor){
        // console.log(selectedColor);
        data["selected"] = selectedColor;
    }
    socket.emit("discard", data);
    //Update cards and pile
    updateCards();
}

function card_click(ev){    
    if(is_drop_allowed(ev.currentTarget.dataset)){
        if (mycards[ev.currentTarget.id]["color"] === "wild") {
            document.querySelector('#color-picker').setAttribute('cardid', ev.currentTarget.id);
            let colorPickerModal = document.getElementById("color-picker-modal");
            colorPickerModal.classList = 'active';
        } else {
            click_drop(ev.currentTarget.id);
        }
    } else {
        M.toast({html: '50RRy, n07 4Ll0weD'})
    }
}

function is_drop_allowed(card){
    let discard = document.getElementById("discardpile").dataset;
    
    // jump in rule    
    if(!myturn){
        return (rules.jumpin && discard.color==card.color && discard.number==card.number && card.color!="wild");
    }
    
    return (discard.color==card.color || discard.number==card.number || card.color=="wild");
}


function card_dragend(ev){
    if(!ev.target || !ev.target.classList || !ev.target.classList.contains("card")) return false;
    
    ev.target.classList.remove("notavail");
    let dropareas = document.getElementsByClassName("droparea");
    for(da of dropareas){
        da.classList.remove("dropoption");
    }
    document.getElementById("discardpile").classList.remove("dropoption");
}




