// Organizes Players before game starts

const Player = require('./player.js');
const Card = require('./card.js');

const util = require('./util.js');
const COLORS = ["blue","green","yellow","red"];

module.exports = class {
    
    constructor(callback, gamecode, players, deckLength){
        this.parent = callback;
        this.gamecode = gamecode;
        this.players = players;
        this.deckLength = deckLength;
        //FIXME bad randomness
        this.playerorder = Object.keys(this.players).sort(() => 0.5-Math.random());
        this.conIds = {};
        
        this.currentPlayer = 0;
        this.direction = 1;
        
        //TODO set rules by user before game starts. Hardcoded here
        // Never m
        this.rules = {jumpin: true,};
        
        this.killtimer;
        this.turntimer;
        this.turninterval;
        this.hasStarted = false;
        
        this.cards = this.createCards();
        this.drawpile = this.spreadCards();
        // check if the first discard is wild and randomly select color
        let firstDiscard = this.drawpile.pop();
        if (this.cards[firstDiscard].color === 'wild') {
            this.cards[firstDiscard].color = COLORS[Math.floor(Math.random()*COLORS.length)];
        }
        this.discard = this.cards[firstDiscard];

        this.createGame(this);
    }
    

    createGame(here){
        console.log("Game '"+this.gamecode+"' created");
        
        const nsp = this.parent.io.of('/game/'+this.gamecode);
        nsp.on('connection', function(socket){
            clearTimeout(here.killtimer);
            
            socket.on('inGame', function(token) {
                here.playerInGame(token, socket);
            });

            socket.on('endGame', function(token) {
                here.playerEndGame(token, socket);
            });
            
            socket.on('disconnect', () => {
                here.onDisconnect(socket);
            });
        
            socket.on('ready', function(token) {
                here.onReady(token, socket);
            });
            
            socket.on('discard', function(data) {
                here.onDiscard(data, socket);
            });
            
            socket.on('newcard', function(data) {
                here.onNewcard(data, socket);
            });

            socket.on('nextplayer', function(data) {
                here.onNextPlayer(data, socket);
            });

            socket.on('sayUnu', function(data) {
                here.onSayUnu(data, socket);
            });

            socket.on('addCardsToPlayer', function(data) {
                here.onAddCardsToPlayer(data, socket);
            });

            socket.on('addCardsToCurrentPlayer', function(num) {
                here.addCardsToCurrentPlayer(num, socket);
            });

            if(!here.hasStarted){
                here.turntimerStart(socket);
                here.hasStarted = true;
            }
        });
    }
    


    playerInGame(token, socket){
        if(token in this.players){
            this.conIds[socket.id] = token;
            this.players[token].inGame = true;
            socket.nsp.emit("playersUpdated", this.getPlayersList());
            console.log(token+" in game");
            
            this.sendPlayerCards(socket);
            this.sendGameData(socket);
        }
        //FIXME else error handling, could have two tabs opened
    }
    
    
    onDisconnect(socket){
        // if no players kill game after some 20min
        if(socket.id in this.conIds){
            let token = this.conIds[socket.id];
            this.players[token].inGame = false;
            // don't remove player
            // delete this.conIds[socket.id];
            socket.nsp.emit("playersUpdated", this.getPlayersList());
            this.sendPlayerCards(socket);
            this.sendGameData(socket);
            console.log(token+" disconnected");
        }
        if(this.getPlayersList().filter(player => player.inGame).length === 0){
            console.log("Last player disconnected. Killtimer started");
            var killtime = 5*60*1000;
            this.killtimer = setTimeout((a) => a.kill(), killtime, this);
        }
    }
    
    
    // Send current state to every user
    sendGameData(socket){
        let gamedata = {
            discard: this.discard,
            players: this.getPlayersList(),
            rules: this.rules,
            drawpile: this.drawpile,
            direction: this.direction,
        };
        socket.nsp.emit("gameData", gamedata);
    }
    
    
    //Send the current cards to the emitting player
    sendPlayerCards(socket){
        let token = this.conIds[socket.id];
        let data = {};
        for(let c of this.players[token].cards){
            data["cardid"+c] = this.cards[c];
        }
        socket.emit("mycards", data);
    }

    onSayUnu(data, socket){
        let token = this.conIds[socket.id];
        this.players[token].saidUnu = data;
        socket.nsp.emit("refresh");
    }
    
    // Game functions
    nextPlayer(socket){
        this.currentPlayer += this.direction;
        
        if(this.currentPlayer == this.playerorder.length){
            this.currentPlayer = 0;
        }else if(this.currentPlayer == -1){
            this.currentPlayer = this.playerorder.length-1;
        }

        this.turntimerStart(socket);        
    }
    
    changeDirection(){
        this.direction *= -1;
    }
    
    getPlayersList(){ 
        let list = [];
        for(let pos in this.playerorder){
            let status = this.players[this.playerorder[pos]].getStatus();
            if(pos == this.currentPlayer) {
                status["current"] = true;
            }
            list.push(status);
        }
        return list;
    }
    
    createCards(){
        let cards = [];
        for (let index = 0; index < this.deckLength; index++) {
            for(let color of COLORS){
                // 1x 0 and 2x 1-9
                for(let i=0;i<=9;i+=.5){
                    cards.push({type:"regular", color:color, number:Math.ceil(i)});
                }
                
                // 2x each special card
                for(let i=0;i<2;i++){
                    cards.push({type:"skip", color:color, number:">"});
                    cards.push({type:"reverse", color:color, number:"R"});
                    cards.push({type:"draw2", color:color, number:"+2"});
                    // double the wilds!!
                    cards.push({type:"wild", color:"wild", number:""});
                }
                
                // Wild+4 cards once
                cards.push({type:"wilddraw4", color:"wild", number:"+4"});
            }
            
        }
        
        return cards;
    }
    
    spreadCards(){
        let cardids = util.shuffle([...this.cards.keys()]);
        for(let token of this.playerorder){
            let playercards = [];
            for(let i=0;i<7;i++){
                playercards.push(cardids.pop());
            }
            this.players[token].cards = playercards;
        } 
        return cardids;
    }
    
    onDiscard(data, socket){
        // Update players handcards
        if ( this.players[this.conIds[socket.id]]) {
            this.players[this.conIds[socket.id]].cards = data.cardorder;  
        } else {
            return false;
        }
        if (data.cardorder.length === 0) {
            this.players[this.conIds[socket.id]].wins = this.players[this.conIds[socket.id]].wins + 1;
            this.players[this.conIds[socket.id]].saidUnu = false;
        }
        let discard = this.cards[data.id];
        discard["id"] = data.id;
        this.discard = discard;
        if("selected" in data && this.cards[data.id]["color"]=="wild"){
            //Wild card played and color sent
            this.discard["color"] = data["selected"];
        }
        
        //TODO validate its the current user or this.rules.jumpin
        // validate user has the card and is it is allowed to be played (but do it above before setting the new discard card ^^)
        
        switch(discard.type) {
            case "skip":
                this.nextPlayer(socket);
                this.cardMessage(socket, "Y0U G0T SK1PP3D!");
                break; 
            case "reverse":
                this.changeDirection();
                // must check the player who would have been next
                // this.cardMessage(socket, "reverse");
                break;
            case "draw2":
                this.nextPlayer(socket);
                this.addCardsToCurrentPlayer(2, socket);
                this.cardMessage(socket, "Y0U G0T B0MB3D!");
                break;
            case "wilddraw4":
                this.nextPlayer(socket);
                this.addCardsToCurrentPlayer(4, socket);
                this.cardMessage(socket, "Y0U G0T QU4DB0MB3D!");
                break;
        }
        this.nextPlayer(socket);
        this.checkIfDeckEmpty();
        socket.nsp.emit("refresh");
    }

    checkIfDeckEmpty() {
        if (this.drawpile.length === 0) {
            //socket.emit("gameFinished", 'GAME OVER');

            // this.cards = this.createCards();
            // this.drawpile = this.spreadCards(true);
        }
    }

    addCardsToCurrentPlayer(num, socket){
        // Update players handcards
        if (this.drawpile.length > 0) {
            var currentPlayerArr = this.getPlayersList().filter(player => player.current);
            if(currentPlayerArr.length){
                let drawpileCardsLeft = this.drawpile.length;
                for (let index = 0; (index < num) && (index < drawpileCardsLeft); index++) {
                    this.players[currentPlayerArr[0]['token']].addCard(this.drawpile.pop());            
                }
                if (this.players[currentPlayerArr[0]['token']].cards.length > 1) {
                    this.players[currentPlayerArr[0]['token']].saidUnu = false;
                }
                this.checkIfDeckEmpty();
                socket.nsp.emit("refresh");
            }
        }        
    }

    onAddCardsToPlayer(data, socket){
        // Update players handcards
        if (this.drawpile.length > 0) {
            if(this.players[data.token]){
                let drawpileCardsLeft = this.drawpile.length;
                for (let index = 0; (index < data.num) && (index < drawpileCardsLeft); index++) {
                    this.players[data.token].addCard(this.drawpile.pop());            
                }
                if (this.players[data.token].cards.length > 1) {
                    this.players[data.token].saidUnu = false;
                }
                this.checkIfDeckEmpty();
                socket.nsp.emit("refresh");
            }
        }        
    }

    onNextPlayer(data, socket){
        this.nextPlayer(socket);
        this.sendGameData(socket);
    }

    turntimerStart(socket){
        var here =  this;
        var turntime = 7*1000;
        //console.log("Turntimer started: "+(turntime / 1000)+" sec");
        clearTimeout(this.turntimer);
        clearInterval(this.turninterval);
        var intervalTime = 7;
        here.timerUpdate(socket, intervalTime);
        this.turninterval = setInterval(function(){
            intervalTime--;
            here.timerUpdate(socket, intervalTime);
        }, 1000);
        this.turntimer = setTimeout((a) => a.turntimerEnd(socket), turntime, this);        
    }

    timerUpdate(socket, time){
        let gamedata = {
            discard: this.discard,
            players: this.getPlayersList(),
            rules: this.rules,
            drawpile: this.drawpile,
            direction: this.direction,
            time: time
        };
        //console.log("timerUpdate: "+time+" sec");
        socket.nsp.emit("timerUpdate", gamedata);
    }

    cardMessage(socket, message){
        let gamedata = {
            players: this.getPlayersList(),
            message: message
        };
        socket.nsp.emit("cardMessage", gamedata);
    }

    turntimerEnd(socket){
        //console.log("turntimer ended, draw card and next player");
        var cardNum = 1;
        var currentPlayerArr = this.getPlayersList().filter(player => player.current);

        this.timerUpdate(socket, 0);
        clearTimeout(this.turntimer);
        clearInterval(this.turninterval);
        
        if(currentPlayerArr.length){
            cardNum  = this.players[currentPlayerArr[0]['token']].cards.length < 1 ? 7 : 1;
        }
        
        this.addCardsToCurrentPlayer(cardNum, socket);
        this.nextPlayer(socket);
        this.sendGameData(socket);
    }
    
    
    kill(){
        clearTimeout(this.killtimer);
        clearInterval(this.turninterval);
        util.killNamespace(this.parent.io, '/game/'+this.gamecode);
        delete this.parent.activeGames[this.gamecode];
        console.log("Killed game "+this.gamecode);
    }

    playerEndGame(token, socket){
        clearTimeout(this.killtimer);
        clearInterval(this.turninterval);
        socket.nsp.emit("gameEnding");
        this.parent.gameToLobby(this.gamecode, this.deckLength);
    }
    
    //XXX debug
    debug(){
        return this.players;
    }
    
}
