// Player class

const util = require('./util.js');

module.exports = class {

    constructor(username, token = false, usertoken = false, createdGame = false){
        this.username = username;
        this.token = token ? token : util.createToken(16, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        this.usertoken = usertoken ? usertoken : util.createToken(8, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        this.inLobby = false;
        this.inGame = false;
        this.ready = false;
        this.saidUnu = false;
        this.createdGame = createdGame;
        this.cards = [];
        this.wins = 0;
    }
    
    addCard(card){
       this.cards.push(card);
    }
    
    hasCard(cardid){
        return this.cards.indexOf(cardid)>=0;
        //return this.cards.some(el=>el.id==card.id);
    }
    
    
    getStatus(){
        return {
            name: this.username,
            usertoken: this.usertoken,
            token: this.token,
            inLobby: this.inLobby,
            inGame: this.inGame,
            ready: this.ready,
            createdGame: this.createdGame,
            cards: this.cards.length,
            saidUnu: this.saidUnu,
            wins: this.wins
        };
    }

}
