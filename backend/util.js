// Utilities

module.exports = {

    //XXX bad randomness
    createToken(length, charPool) {
        let token = "";
        for (let i = 0; i < length; i++) {
            token += charPool[Math.floor(Math.random() * Math.floor(charPool.length))]
        }
        return token;
    },
    
    
    // https://stackoverflow.com/a/36499839/4346956
    killNamespace(io, namespacename) {
        let MyNamespace = io.of(namespacename); // Get Namespace
        for(let socketId in MyNamespace.connected){
            MyNamespace.connected[socketId].disconnect(); // Disconnect Each socket
        }
        MyNamespace.removeAllListeners(); // Remove all Listeners for the event emitter
        delete io.nsps[namespacename]; // Remove from the server namespaces
    },
    
    
    // https://stackoverflow.com/a/2450976/4346956
    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    },
}
