// Organizes Games

const util = require('./util.js');

const Game = require('./game.js');
const Lobby = require('./lobby.js');

module.exports = class {

    constructor(io){
        this.io = io;
        this.activeGames = {};
        this.activeLobbies = {}; 
    }
    
    joinLobby(gamecode, username, token = false, usertoken = false, createdGame = false){
        if(this.lobbyExists(gamecode)){
            return this.activeLobbies[gamecode].newPlayer(username, token, usertoken, createdGame);
        }
    }
    
    createLobby(deckLength){
        let code;
        do{
            code = util.createToken(6, "abcdefghijklmnopqrstuvwxyz0123456789");
        }while(this.lobbyExists(code)||this.gameExists(code))
        this.activeLobbies[code] = new Lobby(this, code, deckLength);
        return code;
    }
    
    lobbyExists(code){
        return code in this.activeLobbies;
    }
    
    
    gameExists(code){
        return code in this.activeGames;
    }
    
    lobbyToGame(code, deckLength){
        let players = this.activeLobbies[code].players;
        for(let key in players) {
            if(!players[key].ready) delete players[key].ready;
        }
        this.activeGames[code] = new Game(this, code, players, deckLength);
        this.activeLobbies[code].kill();
    }
    
    gameToLobby(code, deckLength){
        this.activeLobbies[code] = new Lobby(this, code, deckLength);
        let players = this.activeGames[code].players;
        for(let key in players) {
            this.activeLobbies[code].newPlayer(players[key].username, players[key].token, players[key].usertoken, players[key].createdGame);
        }
        this.activeGames[code].kill();
    }
    
    
    //XXX debug
    debug(gamecode=false){
        if(gamecode && this.lobbyExists(gamecode)){
            return this.activeLobbies[gamecode].debug();
        }else if(gamecode && this.gameExists(gamecode)){
            return this.activeGames[gamecode].debug();
        }
        return {
            lobbies: Object.keys(this.activeLobbies),
            games: Object.keys(this.activeGames)
            };
    }

}
